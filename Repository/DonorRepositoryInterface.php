<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Repository;

use Inisiatif\Component\Contract\Donor\Model\DonorInterface;
use Inisiatif\Component\Contract\Resource\Repository\FilterResourceAwareInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface DonorRepositoryInterface extends FilterResourceAwareInterface
{
    /**
     * @param string $id
     *
     * @return DonorInterface|null
     */
    public function findById(string $id): ?DonorInterface;

    /**
     * @param string $column
     * @param mixed  $value
     *
     * @return DonorInterface|null
     */
    public function findOneBy(string $column, $value): ?DonorInterface;

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function fetch(array $params);
}
