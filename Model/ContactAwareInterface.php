<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ContactAwareInterface
{
    /**
     * @return bool
     */
    public function hasContacts(): bool;

    /**
     * @param DonorInterface $contact
     * @return ContactAwareInterface|self
     */
    public function addContact(DonorInterface $contact);

    /**
     * @param DonorInterface $contact
     * @return ContactAwareInterface|self
     */
    public function removeContact(DonorInterface $contact);

    /**
     * @param DonorInterface $contact
     * @return bool
     */
    public function hasContact(DonorInterface $contact): bool;

    /**
     * @return DonorInterface|null
     */
    public function getDefaultContact(): ?DonorInterface;
}
