<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Model;

use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;
use Inisiatif\Component\Contract\Resource\Model\IdentificationNumberAwareInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface DonorInterface extends ResourceInterface, IdentificationNumberAwareInterface
{
    public const TYPE_PERSONAL = 'PERSONAL';

    public const TYPE_COMPANY = 'COMPANY';

    public const TYPE_COMMUNITY = 'COMMUNITY';

    public const DONOR_TYPES = [
        self::TYPE_COMMUNITY,
        self::TYPE_COMPANY,
        self::TYPE_PERSONAL,
    ];

    /**
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * @param string|null $value
     * @return DonorInterface|self
     */
    public function setType(?string $value): self;

    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string|null $value
     * @return DonorInterface|self
     */
    public function setName(?string $value): self;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @param string|null $value
     * @return DonorInterface|self
     */
    public function setEmail(?string $value): self;
}
