<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface DonorAwareInterface
{
    /**
     * @return DonorInterface|null
     */
    public function getDonor(): ?DonorInterface;

    /**
     * @param DonorInterface|null $donor
     * @return DonorAwareInterface|self
     */
    public function setDonor(?DonorInterface $donor);
}
