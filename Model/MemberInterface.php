<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Model;

use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface MemberInterface extends ResourceInterface
{
    /**
     * @return DonorInterface|null
     */
    public function getMember(): ?DonorInterface;

    /**
     * @param DonorInterface|null $member
     * @return MemberInterface|self
     */
    public function setMember(?DonorInterface $member): self;

    /**
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * @param string|null $type
     * @return MemberInterface|self
     */
    public function setType(?string $type): self;

    /**
     * @return bool
     */
    public function isDefault(): bool;

    /**
     * @param bool $isDefault
     * @return MemberInterface|self
     */
    public function setIsDefault(bool $isDefault): self;
}
