<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface MemberAwareInterface
{
    /**
     * @return bool
     */
    public function hasMembers(): bool;

    /**
     * @param DonorInterface $member
     * @return MemberAwareInterface|self
     */
    public function addMember(DonorInterface $member);

    /**
     * @param DonorInterface $member
     * @return MemberAwareInterface|self
     */
    public function removeMember(DonorInterface $member);

    /**
     * @param DonorInterface $member
     * @return bool
     */
    public function hasMember(DonorInterface $member): bool;

    /**
     * @return DonorInterface|null
     */
    public function getDefaultMember(): ?DonorInterface;
}
