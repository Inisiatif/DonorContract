<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Model;

use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface MemberTypeInterface extends ResourceInterface
{
    /**
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * @param string|null $type
     * @return MemberTypeInterface|self
     */
    public function setType(?string $type): self;
}
