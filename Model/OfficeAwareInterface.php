<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface OfficeAwareInterface
{
    /**
     * @return DonorInterface|null
     */
    public function getOffice(): ?DonorInterface;

    /**
     * @param DonorInterface|null $office
     * @return OfficeAwareInterface|self
     */
    public function setOffice(?DonorInterface $office);
}
