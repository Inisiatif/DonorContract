<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Donor\Service;

use Inisiatif\Component\Contract\Donor\Model\DonorInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface DonorServiceInterface
{
    /**
     * @param DonorInterface $donor
     * @return DonorInterface
     */
    public function save(DonorInterface $donor): DonorInterface;
}
